export class HtmlBuilder {
    constructor() {
        this.element = null;
    }

    setElement(element) {
        this.element = element;
    }

    build() {
        return this.element.build();
    }
}