export class HtmlElement {
    constructor(id, tagName) {
        this.id = id;
        this.tagName = tagName;

        this.content = '';

        this.classList = [];

        this.children = [];
    }

    addClass(className) {
        this.classList.push(className);
        return this;
    }

    addChild(htmlElement) {
        this.children.push(htmlElement);
        return this;
    }

    setContent(content) {
        this.content = content;
        return this;
    }

    hasChild() {
        return this.children.length;
    }

    build() {
        const element = document.createElement(this.tagName);
        element.setAttribute('id', this.id);
        if (this.classList.length) {
            this.classList.forEach((className) => {
                element.classList.addClass(className);
            })
        }
        if (this.hasChild()) {
            this.children.forEach((htmlElement) => {
                element.appendChild(htmlElement.build());
            })
        }
        return element;
    }
}