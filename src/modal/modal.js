import { HtmlBuilder } from "../_helpers/html-builder.js";
import { HtmlElement } from "../_helpers/html-element.js";

export class Modal {
    constructor() {
        this.builder = new HtmlBuilder();

        this.modal = new HtmlElement('outer-modal', 'div');
        this.modal
            .addClass('outer-modal');
        
        const inner = new HtmlElement('inner-modal', 'div');
        inner
            .addClass('inner-modal')
            .setContent('Application is loading... Please wait...');
        this.modal.addChild(inner);

        this.builder.addElement(this.modal);

    }

    open() {
        const body = document.querySelector('body');

        body.appendChild(this.builder.build());
    }

    close() {
        this.modal.remove();
    }
    
}